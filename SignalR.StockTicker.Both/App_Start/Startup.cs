﻿using Owin;

namespace SignalR.StockTicker.Both
{
    public class Startup
    {
        public void Configuration(IAppBuilder appBuilder)
        {
            appBuilder.MapSignalR();
        }
    }
}