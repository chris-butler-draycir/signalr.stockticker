﻿// A simple templating method for replacing placeholders enclosed in curly braces.
if (!String.prototype.supplant) {
    String.prototype.supplant = function (o) {
        return this.replace(/{([^{}]*)}/g,
            function (a, b) {
                var r = o[b];
                return typeof r === 'string' || typeof r === 'number' ? r : a;
            }
        );
    };
}

$(function () {
    var up = '▲',
        down = '▼',
        $stockTable = $('#stockTable'),
        $stockTableBody = $stockTable.find('tbody'),
        rowTemplate = '<tr data-symbol="{Symbol}"><td>{Symbol}</td><td>{Price}</td><td>{DayOpen}</td><td>{Direction} {Change}</td><td>{PercentChange}</td></tr>';

    function formatStock(stock) {
        return $.extend(stock, {
            Price: stock.Price.toFixed(2),
            PercentChange: (stock.PercentChange * 100).toFixed(2) + '%',
            Direction: stock.Change === 0 ? '' : stock.Change >= 0 ? up : down
        });
    };

    var updateStockPrice = function(stock) {
        var displayStock = formatStock(stock),
            $row = $(rowTemplate.supplant(displayStock));

        $stockTableBody.find('tr[data-symbol=' + stock.Symbol + ']')
            .replaceWith($row);
    };

    var getInitialStock = function(stocks) {
        $stockTableBody.empty();
        $.each(stocks,
            function() {
                var stock = formatStock(this);
                $stockTableBody.append(rowTemplate.supplant(stock));
            });
    };

    var connection = $.hubConnection("http://localhost:6822/signalr", { useDefaultPath: false });
    var stockTickerProxy = connection.createHubProxy('stockTickerMini');
    stockTickerProxy.on('updateStockPrice', updateStockPrice);

    function init() {
        stockTickerProxy.invoke('getAllStocks').done(getInitialStock);
    }

    // Start the connection
    connection.start().done(init);
});